import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  ViewChild,
  Input
} from "@angular/core";

@Component({
  selector: "app-dice",
  templateUrl: "./dice.component.html",
  styleUrls: ["./dice.component.css"]
})
export class DiceComponent implements OnInit {
  @ViewChild("dice", { static: false }) dice: any;
  @ViewChild("player1Dice", { static: false }) player1Dice: DiceComponent;

  @Output() rolledDice: EventEmitter<any> = new EventEmitter<any>();
  @Input() active: boolean = false;
  @Input() disabled: boolean = false;
  lastRoll: number = 0;
  lastShowClass: string = "";

  constructor() {}

  ngOnInit() {}

  rollRandom() {
    if (this.active) {
      let diceRoll: number = Math.floor(Math.random() * 6 + 1);
      this.rollDice(diceRoll);
    }
  }

  rollDice(roll: number) {
    this.dice.nativeElement.className = "dice dice-one";

    let className: string = "show-" + roll;
    if (roll == this.lastRoll && !this.lastShowClass.includes("alt")) {
      className += "-alt";
    }

    this.dice.nativeElement.classList.add(className);
    this.lastRoll = roll;
    this.lastShowClass = className;

    this.rolledDice.emit(roll);
  }
}
