import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-board",
  templateUrl: "./board.component.html",
  styleUrls: ["./board.component.css"]
})
export class BoardComponent implements OnInit {
  tiles: any;

  diceValue: number;

  chips: any = {};

  currentId: number = 1;

  player: any = {};

  players: any = {
    player1: {},
    player2: {},
    player3: {},
    player4: {}
  };

  dices: any = {
    dice1: {},
    dice2: {},
    dice3: {},
    dice4: {}
  };

  zIndexCount: number = 1;

  currentTurn: number = 1;
  currentPendingRoll: any;

  test: boolean = false;

  isGameFinished: boolean = false;

  constructor() {}

  ngOnInit() {
    this.initPlayer(1, "#0071f0");
    this.initPlayer(2, "#ff2c01");
    this.initPlayer(3, "#fefe01");
    this.initPlayer(4, "#01c03e");

    this.player.number = 1;
  }

  initPlayer(playerNumber, color) {
    let player: any = {
      ready: true,
      number: playerNumber,
      wins: []
    };
    this.players["player" + playerNumber] = player;

    this.initPlayerColor(playerNumber, color);
  }

  getStartingTileByChip(chip: any) {
    let startingTile: any = {};
    let tile: any = {};
    tile.number = this.getStartingTileNumberByPlayer(chip.playerNumber);
    tile.id = "tile" + tile.number;

    let tileHtml = document.getElementById(tile.id);

    startingTile = {
      tile: tile,
      tileHtml: tileHtml
    };

    return startingTile;
  }

  releaseChip(chip: any, diceValue: number) {
    chip.currentTile = this.getStartingTileByChip(chip);
    this.moveChipToTile(chip, chip.currentTile.tileHtml);

    this.zIndexCount += 1;

    chip.released = true;
    setTimeout(() => {
      this.onChipReachedGoal(chip, diceValue);
    }, 450);
  }

  getNextTileId(chip: any, tileNumber: number) {
    let prefix: string = "tile";

    if (
      chip.isThroughColorEntrance &&
      tileNumber >= this.getColorEntranceTileByPlayerNumber(chip.playerNumber)
    ) {
      prefix = `tileColor${chip.playerNumber}_`;
    }

    return prefix + tileNumber;
  }

  getNextTile(chip: any, tileNumber: number, addUp: number) {
    let tile: any = {};

    tile.number = tileNumber + addUp;
    if (chip.playerNumber != 1) {
      if (tileNumber + addUp == 41) {
        tile.number = 1;
        tileNumber = 0;
      }
    }
    if (
      chip.currentTile.tile.id &&
      chip.isThroughColorEntrance &&
      chip.isInFinalTile &&
      addUp != -1
    ) {
      tile.id = "final-tile";
    } else {
      tile.id = this.getNextTileId(chip, tileNumber + addUp);
    }

    let nextTileHtml = document.getElementById(tile.id);

    return {
      tileHtml: nextTileHtml,
      tile: tile
    };
  }

  moveChip(chip: any) {
    if (
      (chip.playerNumber == this.player.number &&
        this.currentTurn == this.player.number &&
        !this.player.movedChip &&
        this.players["player" + chip.playerNumber].rolledDice) ||
      this.test
    ) {
      let playerDiceRoll: number = this.players["player" + chip.playerNumber]
        .diceValue;

      this.player.movedChip = true;

      if (
        (!chip.released && playerDiceRoll == 6) ||
        (this.test && !chip.released)
      ) {
        this.releaseChip(chip, playerDiceRoll);
      } else {
        chip.sameTileNumber = "";
        this.moveChipThroughTiles(
          chip,
          playerDiceRoll,
          chip.currentTile.tile.number + playerDiceRoll
        );
      }
    }
  }

  moveChipThroughTiles(
    chip: any,
    diceValue: number,
    nextNum: number,
    currVal: number = 0,
    initialized: boolean = false
  ) {
    let currValGoal: number = diceValue;
    let finalTileNum: number = this.getFinalTileNumberByPlayer(
      chip.playerNumber
    );

    let addUp: number = 1;
    let currTileNumber: number = chip.currentTile.tile.number;
    let nextTileNumber: number = currTileNumber + addUp;
    if (
      nextTileNumber ==
      this.getColorEntranceTileByPlayerNumber(chip.playerNumber)
    ) {
      chip.isThroughColorEntrance = true;
    }

    if (nextNum > finalTileNum && chip.isThroughColorEntrance) {
      currValGoal = nextNum - finalTileNum;
      if (!initialized) {
        currVal = currValGoal - diceValue;
      }

      addUp = currVal < 0 ? 1 : -1;
    }

    if (currVal < currValGoal) {
      chip.isInFinalTile =
        nextTileNumber == this.getFinalTileNumberByPlayer(chip.playerNumber);
      chip.currentTile = this.getNextTile(chip, currTileNumber, addUp);
      this.moveChipToTile(chip, chip.currentTile.tileHtml);

      currVal++;
      setTimeout(() => {
        this.moveChipThroughTiles(chip, diceValue, nextNum, currVal, true);
      }, 400);
    } else {
      this.zIndexCount += 1;
      this.onChipReachedGoal(chip, diceValue);
    }
  }

  onChipReachedGoal(chip: any, diceValue: number) {
    if (this.isChipFinished(chip)) {
      this.chipFinished(chip);
    } else {
      this.expulseEnemyChipsFromTile(this.getEnemyChipsOnTile(chip), chip);
      this.getHowManyChipsOnTile(chip);
    }
    if (diceValue == 6 || diceValue == 1) {
      this.repeatTurn(chip.playerNumber);
    } else {
      this.finishTurn(chip.playerNumber);
    }
  }

  chipFinished(chip) {
    chip.exiting = true;
    this.players["player" + chip.playerNumber].wins.push({});
    setTimeout(() => {
      chip.finished = true;
      if (this.players["player" + chip.playerNumber].wins.length == 4) {
        this.isGameFinished = true;
        alert("Jugador " + chip.playerNumber + " ganó el juego!");
      }
    }, 1000);
  }

  repeatTurn(playerNumber) {
    this.player.movedChip = false;
    this.players["player" + playerNumber].rolledDice = false;
    this.currentPendingRoll = this.getPendingRoll(this.player.number);
  }

  finishTurn(playerNumber) {
    if (!this.test) {
      this.player.number = this.player.number == 4 ? 1 : this.player.number + 1;
      this.currentTurn = this.currentTurn == 4 ? 1 : this.currentTurn + 1;
      this.player.movedChip = false;
      this.players["player" + playerNumber].rolledDice = false;
      this.currentPendingRoll = this.getPendingRoll(this.player.number);
    }
  }

  moveChipToTile(chip: any, tileHtml: any) {
    chip.style.top = tileHtml.offsetTop + tileHtml.offsetHeight / 4 - 2 + "px";
    chip.style.left = tileHtml.offsetLeft + tileHtml.offsetWidth / 4 - 3 + "px";
    chip.style.zIndex = this.zIndexCount;
  }

  getStartingTileNumberByPlayer(playerNumber: number) {
    let tileNumber: number;

    switch (playerNumber) {
      case 1:
        tileNumber = 2;
        break;
      case 2:
        tileNumber = 12;
        break;
      case 3:
        tileNumber = 32;
        break;
      case 4:
        tileNumber = 22;
        break;
      default:
        break;
    }

    return tileNumber;
  }

  getFinalTileNumberByPlayer(playerNumber: number) {
    let tileNumber: number;

    switch (playerNumber) {
      case 1:
        tileNumber = 45;
        break;
      case 2:
        tileNumber = 15;
        break;
      case 3:
        tileNumber = 35;
        break;
      case 4:
        tileNumber = 25;
        break;
      default:
        break;
    }

    return tileNumber;
  }

  isTileThroughColorEntrance(tileId: string) {
    return tileId.includes("tileColor");
  }

  getColorEntranceTileByPlayerNumber(playerNumber: number) {
    let tileNumber: number;

    switch (playerNumber) {
      case 1:
        tileNumber = 41;
        break;
      case 2:
        tileNumber = 11;
        break;
      case 3:
        tileNumber = 31;
        break;
      case 4:
        tileNumber = 21;
        break;
      default:
        break;
    }

    return tileNumber;
  }

  getNewChips(color: string, playerNumber) {
    let chips = [];
    for (let index = 0; index < 4; index++) {
      let chip = {
        id: index + 1,
        playerNumber: playerNumber,
        style: {
          background: color
        },
        currentTile: {
          tile: {}
        }
      };
      this.initChipPosition(chip);
      chips.push(chip);
    }
    return chips;
  }

  getHowManyChipsOnTile(chip) {
    let countTiles: number = this.chips[`player${chip.playerNumber}`].filter(
      c => c.currentTile.tile.id == chip.currentTile.tile.id
    ).length;

    if (countTiles > 1) {
      chip.sameTileNumber = countTiles;
    } else {
      chip.sameTileNumber = "";
    }
  }

  initPlayerColor(playerNumber: number, color: string = "") {
    let colorTiles: any = document.getElementsByClassName(
      `player-color-${playerNumber}`
    );

    for (let index = 0; index < colorTiles.length; index++) {
      colorTiles[index].style.background = color;
    }

    this.players[`player${playerNumber}`].color = color;
    this.chips[`player${playerNumber}`] = this.getNewChips(color, playerNumber);
  }

  initChipPosition(chip) {
    let initCell: any = document.getElementById(
      "player-" + chip.playerNumber + "-chip-" + chip.id
    );

    let offsetParent: any = initCell.offsetParent;

    chip.style.top =
      offsetParent.offsetTop +
      initCell.offsetTop +
      initCell.offsetHeight / 4 -
      3 +
      "px";
    chip.style.left =
      offsetParent.offsetLeft +
      initCell.offsetLeft +
      initCell.offsetWidth / 4 -
      4 +
      "px";
    chip.style.zIndex = this.zIndexCount;

    //this.moveChipToTile(chip, initCell);
  }

  cloneObject(object: any) {
    return JSON.parse(JSON.stringify(object));
  }

  getEnemyChipsOnTile(chip) {
    let enemyChips: Array<any> = [];
    for (let key in this.chips) {
      this.chips[key].forEach(c => {
        if (
          c.currentTile.tile.id == chip.currentTile.tile.id &&
          chip.playerNumber != c.playerNumber
        ) {
          enemyChips.push(c);
        }
      });
    }

    return enemyChips;
  }

  expulseEnemyChipsFromTile(enemyChips: any, chip) {
    if (enemyChips.length > 0) {
      chip.jumping = true;
      setTimeout(() => {
        enemyChips.forEach(c => {
          c.currentTile.tile = {};
          c.released = false;
          c.sameTileNumber = "";
          chip.jumping = false;
          this.initChipPosition(c);
        });
      }, 570);
    }
  }

  isChipFinished(chip) {
    return (
      chip.currentTile.tile.number ==
        this.getFinalTileNumberByPlayer(chip.playerNumber) &&
      chip.currentTile.tile.id == "final-tile"
    );
  }

  rolledDice(playerNumber, roll) {
    if (this.currentPendingRoll) {
      clearInterval(this.currentPendingRoll);
      this.setDiceBoxShadow(playerNumber, "none");
    }
    let player: any = this.players["player" + playerNumber];
    player.diceValue = roll;
    player.rolledDice = true;
    if (!this.hasPlayerChipsReleased(playerNumber) && roll != 6) {
      setTimeout(() => {
        this.finishTurn(playerNumber);
      }, 2000);
    }
  }

  hasPlayerChipsReleased(playerNumber: number) {
    return (
      this.chips["player" + playerNumber].filter(c => c.released && !c.finished)
        .length > 0
    );
  }

  getPendingRoll(playerNumber: number) {
    this.setDiceBoxShadow(
      playerNumber,
      "0 0 100px 50px " + this.players[`player${playerNumber}`].color
    );
    setTimeout(() => {
      this.setDiceBoxShadow(playerNumber, "none");
    }, 1000);
    return setInterval(() => {
      this.setDiceBoxShadow(
        playerNumber,
        "0 0 100px 50px " + this.players[`player${playerNumber}`].color
      );
      setTimeout(() => {
        this.setDiceBoxShadow(playerNumber, "none");
      }, 1000);
    }, 2000);
  }

  setDiceBoxShadow(playerNumber: number, boxShadow: string) {
    this.dices["dice" + playerNumber].style = {
      boxShadow: boxShadow,
      transition: "1s"
    };
  }
}
